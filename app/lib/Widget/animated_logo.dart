import 'package:flutter/material.dart';

class AnimatedLogo extends AnimatedWidget {
  // Make the Tweens static because they don't change.
  static final _positionTween = Tween<double>(begin: 0, end: 500);
  static final _opacityTween = Tween<double>(begin: 1, end: 0);

  AnimatedLogo({Key key, Animation<double> animation})
      : super(key: key, listenable: animation);

  Widget build(BuildContext context) {
    final animation = listenable as Animation<double>;
    return Scaffold(
      backgroundColor: Colors.orange[800],
      body: Stack(
        alignment: Alignment.center,
        children: [
          Positioned(
            bottom: 0,
            child: Opacity(
              opacity: _opacityTween.evaluate(animation),
              child: Container(
                  height: 100,
                  width: 200,
                  child: Image.asset("assets/splash.png")),
            ),
          ),
          Positioned(
              bottom: _positionTween.evaluate(animation),
              child: Container(
                  height: 100,
                  width: 100,
                  child: Image.asset("assets/Tennis_ball.png"))),
        ],
      ),
    );
  }
}
